'use strict';
//Popup
function POPUP_FEATURE() {
  var _wrap = "<div class='layout-popup-wrap'></div>",
      _bg = "<div class='layout-popup-background'><div class='popup-btn-close'><i></i><i></i></div></div>";
  this.popupWrap = $('<div />');
  this.popupWrap.html(_wrap + _bg);
}
POPUP_FEATURE.prototype = {
  popup: function(content) {
    this.popupWrap.find('.layout-popup-wrap').html(content);
  },
  popupOpen: function() {
    var event = jQuery.Event("popupOpen");
    // event.name = "wdjfioejfiejifje"
    $('body')
      .append(this.popupWrap.html())
      .trigger(event)
  },
  popupClose: function() {
    var event = jQuery.Event("popupClose");
    $('body')
      .find('.layout-popup-background, .layout-popup-wrap')
      .fadeOut(300, function() { $(this).remove(); })
      // .remove()
      .end()
      .trigger(event)
  }
}
var POPMsg = new POPUP_FEATURE();

  //Popup For Login Actions
  // popLogin();
  function popAlert(mytext){
    var mystring = mytext.toString();
    $("body").find(".layout-popupalert-wrap,.layout-popupalert-background").remove();
    $("body").append('<div class="layout-popupalert-wrap"><div class="layout-popupalert-box"><i class="btn-popclose"></i><div class="layout-popupalert-content">'+mystring+'</div></div></div><div class="layout-popupalert-background"></div>');
  }

$(window).scroll(function () {
  var scrollVal = parseInt($(this).scrollTop());
  if(scrollVal>0){
      $("#header").addClass('is-scrolled');
  }else{
      $("#header").removeClass('is-scrolled');
  }
  // scrollNow = scrollVal;
});

function bannerAnimate(){
  $(".fix-banner").find(".space").text("").delay(600).queue(
    function(next){
      $(this).text("D");
    next();
  }).delay(300).queue(
    function(next){
      $(this).text("De");
    next();
  }).delay(300).queue(
    function(next){
      $(this).text("Des");
    next();
  }).delay(300).queue(
    function(next){
      $(this).text("Desi");
    next();
  }).delay(300).queue(
    function(next){
      $(this).text("Desir");
    next();
  }).delay(300).queue(
    function(next){
      $(this).text("Desire");
    next();
  }).delay(600).queue(
  function(next){
    $(this).text("Desir");
  next();
  }).delay(150).queue(
    function(next){
      $(this).text("Desi");
    next();
  }).delay(150).queue(
    function(next){
      $(this).text("Des");
    next();
  }).delay(150).queue(
    function(next){
      $(this).text("De");
    next();
  }).delay(150).queue(
    function(next){
      $(this).text("D");
    next();
  }).delay(150).queue(
    function(next){
      $(this).text("");
    next();
  }).delay(600).queue(
  function(next){
    $(this).text("享");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("享樂");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("享樂其");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("享樂其中");
  next();
}).delay(600).queue(
  function(next){
    $(this).text("享樂其");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("享樂");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("享");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("");
  next();
}).delay(600).queue(
  function(next){
    $(this).text("美");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("美好");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("美好放");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("美好放縱");
  next();
}).delay(600).queue(
  function(next){
    $(this).text("美好放");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("美好");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("美");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("");
  next();
}).delay(600).queue(
  function(next){
    $(this).text("不");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("不絕");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("不絕對");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("不絕對慾");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("不絕對慾幸");
  next();
}).delay(300).queue(
  function(next){
    $(this).text("不絕對慾幸戀");
  next();
}).delay(600).queue(
  function(next){
    $(this).text("不絕對慾幸");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("不絕對慾");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("不絕對");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("不絕");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("不");
  next();
}).delay(150).queue(
  function(next){
    $(this).text("");
    bannerAnimate();
  next();
})
}

//on Document Ready
(function() {
  bannerAnimate();
  // popAlert("MY ALERT!");
  // var scrollVal = parseInt($(this).scrollTop());
  // if(scrollVal>0){
  //     $("#header").addClass('is-scrolled');
  // }else{
  //     $("#header").removeClass('is-scrolled');
  // }

  $("body").on('click', '.btn-refresh', function (event) {
      event.preventDefault();
      /* Act on the event */
      location.reload();
  });
  
  $("body").on('click', '.header-left,.header-menu li', function(event) {
    // event.preventDefault();
    /* Act on the event */
    if( $("body").hasClass('is-croping') ){
      location.reload();
    }
    else if( ! $("body").hasClass('is-slideup-footer') &&  ! $("body").hasClass('is-croping') ){
      $("#header").toggleClass('is-open');
    }else{
      $("body").removeClass('is-slideup-footer');
      $("body").removeClass("is-croping");
      $("#popcrop").removeClass('is-show');
    }
  });

  $("body").on('click', '.open-tvc', function() {
    var popupContent = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/JRfuAukYTKg?autoplay=1" frameborder="0" allowfullscreen></iframe>';
    POPMsg.popupClose();  
    POPMsg.popup(popupContent);
    POPMsg.popupOpen();
  });

  $("body").on('click', '.layout-popup-background,.popup-btn-close', function() {
    POPMsg.popupClose();
  });

  //popAlert
  $("body").on('click', '.layout-popupalert-background,.layout-popupalert-wrap .btn-popclose', function() {
    $("body").find(".layout-popupalert-wrap,.layout-popupalert-background").remove();
  });

  // 執行 FastClick
  FastClick.attach(document.body);

})();